module ApplicationHelper
    # returns the full title on a per-page basis 
    
    def full_title(page_title = '')
        if page_title.empty?
            page_title = 'huh'
        else
            page_title
        end
    end
end
